# TM (TamperMonkey) Scripts

本项目收录本人常用的TamperMonkey脚本

### video-downloader.user.js

本脚本利用Aria2(或Motix等基于Aria2的下载工具)来帮助您从微信公众号推文、抖音、快手、西瓜视频、CCTV、YouTube等网站下载视频。
