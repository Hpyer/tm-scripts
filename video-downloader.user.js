// ==UserScript==
// @name         视频下载
// @namespace    hpy-video-downloader
// @version      1.0.5
// @description  本脚本利用Aria2(或Motix等基于Aria2的下载工具)来帮助您从微信公众号推文、抖音、快手、西瓜视频、CCTV、YouTube等网站下载视频。
// @author       Hpyer
// @updateURL    https://gitlab.com/Hpyer/tm-scripts/-/raw/main/video-downloader.user.js
// @downloadURL  https://gitlab.com/Hpyer/tm-scripts/-/raw/main/video-downloader.user.js
// @icon         data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAB1RJREFUeF7tm3+MFVcVx79nHiINxe6bLZSmVpfum11/ICFp+iNpUEhqa1NpNWiURKumwnZmgETS/tPECElNTETR0jevLaalbUyMbUlMjVgiv43EglHxB92dV9kIqATefduWn2XfnHYWHu6+N3fm3pnZ3TSw/23m3PPjM/eec++Z+wiX+R9d5vHjCoArM+AyJzBpS+DDPz581alpp6yQ//Sz0/0jq288MxnvYkIAzCxXSwHhHgbPY6BEQBj4DS0BH2XAJ6BKxH9jot+Jh6x/jjeUcQNglv27QPxZAHcBNC9dIDQI8DYw9p6fEvzq7b7eE+n0yEflDqDT6/8Gg1yAbsnXWRoAYZ2wSxvz1JsbALMysISZXAIW5elgmy7Gb4mDdbUVvdvysJMZwDXegWIBV4VvZUkeDinrIKwXtrVaWV4imAnAtd7rvQEKzwG4LYUjZ0EYBGMIQBeA2fo6eF+jQEvf7LPe0B97YURqAEWvfwHB2K1o+C0Ae5loOzHvLvCUwePunP+NHtv17KFpb598p4sNYy4R7mfgCwCuVtHPREvrdukXKrKtMqkAmOX+NSDje0kGCXglIOPp6WembtOt8zPLf796GFMXG0RLGVicZAuE1cK21ifKtQhoA+is+F9nxvNxhpqB1+3uX+s6FCVfrLzxeYOD5YkgGHcL19qqY1MLQLFSvYOYfx9ngGHYdaf7SR0nVGVNz/8ygF/GyjPdIdzSH1R1KgO49qnB6zk4v4cZ3VLlmsZVnRwtF84G4uAVuQ84wlS4t+7cdEBFvzIAszywGURflCkt8LkZx925J1WMZpUJX0bQOP8fOQT+jXB77lWxowQg3OSA6SWZQgr49tqKnj+qGMxLZtbGf103fL4xppKM0U34irCt+OWiWgaLnr9dusNjrBaufvbNA8SF8wZejdTF2C1c6zNJdhJnwIW9vbEp2kiwVri9a5KMjOdzs1x9GMQ/jLRBeFDY1jMJFSvePdMbeC36YEP7zGFeUF1lnVMNsLPSf0PN7j2qKq8qV/T8/e+V3pvb5XmfcHpuTQ2gY4O/0ChgR5QCMowltYe6N2s4+X0CHgVjfwBeNuT2/EV1bJJc0asuJ/BTkqUQuzeIXQLSHR/hGWFbDyY51nxe9PwdBCwcLc9Aue5YK1R1JMlJZwFho7Ct5dIEHqfY9Pwws7dPIc0dVxSA0C4B50BYVrOtF5ICTHreWamuYuaftskRhLCtTm0AHZWDXQZPOdQ+kAaFU5qT5NDo5zIA/5fhPYwpK1Q3L1G2w7Zbg9iPTob8JWH3vBy5lGWBdHq+y8ATEc9/IhzrO/kCaGrjdcLpeURH92hZs+LvAuPTbeMZPxKu9bAWANPzwybHt1sHBQ0sGlpp7dRxMnkGjNE2RIRVaZZFp1ddyeDHW30jxs9rrvU1LQAypwManjNkf3xwHAE0VW9lFB7RWRYd5YH5BtGf2/MAbRN26U4tAKbnH4loXUM4VuLmqdWQ5gxoGa6+LDrWH+owPjhcjwj0H8Kx5ioDCD9anJ529nQeCTDUkQ0AAMaxIMBXVZae6fkhgI6xvrMQTk9kJYh8m0Xv9XmEwl9bATCws+5Y2l3fzAAAqOYe0/PDJTC/1XfZzNUCAOiXwFxmgAYAGex3hjHr5CrreFuCjFoX8iWAScgBeksg3L7LEnTUEpImNFkSnMAqcDGO9iTYGqRKbpBBkQKQlsHx3wfElkGzUn0WzN9sL3VYI2xrrU55DmXjZkDkRojA3605PY/pGNJMgtKN0MzyodkNGv6vxLa01MX5KgUQsxV+VTjW58YHQHzNjzueh/6k2aNIAUgPQ8ynPjDjQ9cde2D2KVUIyTNA7TA0oQDC4GTHYWbcV3cteWu6hUxex+GJByD/BLZVONbdWWZAmobIhAOIbYkRHtA5sRU9P3NLbMIBXFgGsqYoXhOOpfVZPGtTdFIAxLXFCajUHMtRXQpZ5SYFwMW9vPTDCIP66k7p6azBqYyfNABJn8bS1N+kgMNgqYC2Owit3eXResLT6uj/iYNdgRFsimvgKDc3kj6OcsH4aL2v+99Jgak8N8vVG0Gciy4QbRJ26VvaZ4HWASqfx5mNBXW3O/b+gAqAzg39H+OCcVBFVkHmsHCsj2QGMJILFC5IAFgmHOtnCo7FipievxfA7Vn1ENPKmluK6m6PqFZeAk1HVK7IgLAbjCeEY72YNgDzSf8TCEZug3wyrQ5w8sdbbQAjewPFS1LvXZ/bQqAtBmPLcbdU1Q0kEwSF4FPNgGYQmtfkwmFbGLQDFBwsGFP/dKKvS3asHcMpFQTF4DMBCAdnuihJOMKMkVnBDayN6+poQdAIPjOAUEEeV2VVOr5KEDSDzwVAc75muSytAmAk98QlxhTB5wrgUpVIcV1eFYAUQsrgxwXApRmh8YMJHQBNCBTgBwz6FLjxXJZ7SqnKoG45S/rJjC4AXftx8hMCIMqB5o+muFEww+dZevtZgEwagCxO5zn2CoA8ab4fdV2ZAe/Ht5anz+8CpaFibidGlD0AAAAASUVORK5CYII=
// @match        *://mp.weixin.qq.com/*
// @match        *://*.kuaishou.com/*
// @match        *://*.ixigua.com/*
// @match        *://*.youtube.com/*
// @match        *://*.cctv.com/*
// @match        *://*.cctv.cn/*
// @grant        unsafeWindow
// @grant        GM_registerMenuCommand
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @grant        GM_download
// @grant        GM_xmlhttpRequest
// @connect      47.99.158.118
// @connect      mpvideo.qpic.cn
// @connect      localhost
// ==/UserScript==

(function () {
  'use strict';

  const domHead = document.getElementsByTagName('head')[0];
  const domStyle = document.createElement('style');
  domStyle.rel = 'stylesheet';

  /*--config--*/
  const Config = {
    id: 'hpyVideoDownloader',
    iconTop: 160,
    iconWidth: 40,
    rpcUrl: GM_getValue('hpyVideoDownloaderRpcUrl', 'http://localhost:6800/jsonrpc'),
    rpcToken: GM_getValue('hpyVideoDownloaderRpcToken', ''),
  }

  const {
    id,
    iconTop,
    iconWidth,
  } = Config;

  const Webs = [
    { name: 'kuaishou', match: /^https?:\/\/www\.kuaishou\.com\/?.+$/ },
    { name: 'xigua', match: /^https?:\/\/www\.ixigua\.com\// },
    { name: 'youtube', match: /^https?:\/\/www\.youtube\.com/ },
    { name: 'cctv', match: /^https?:\/\/.*\.cctv\.[com|cn]/ },
    { name: 'weixinMp', match: /^https?:\/\/mp\.weixin\.qq\.com/ },
  ];

  /** 工具类 */
  class Utils {
    static request(method, url, data, isCookie = '') {
      let request = new XMLHttpRequest();
      return new Promise((resolve, reject) => {
        request.onreadystatechange = function () {
          if (request.readyState == 4) {
            if (request.status == 200) {
              resolve(request.responseText);
            } else {
              reject(request.status);
            }
          }
        }
        request.open(method, url);
        //request.withCredentials = true;
        if (isCookie) {
          request.withCredentials = true;
        }
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send(data);
      });
    }

    static setCookie(cname, cvalue, exdays) {
      let d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
      let expires = "expires=" + d.toGMTString();
      document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    static getCookie(cname) {
      let name = cname + "=";
      let ca = document.cookie.split(';');
      for (let i = 0; i < ca.length; i++) {
        let c = ca[i].trim();
        if (c.indexOf(name) == 0) { return c.substring(name.length, c.length); }
      }
      return "";
    }

    static getQueryString(e) {
      let t = new RegExp("(^|&)" + e + "=([^&]*)(&|$)");
      let a = window.location.search.substr(1).match(t);
      if (a != null) return a[2];
      return "";
    }

    static getUrlParams(url) {
      let reg = /([^?&+#]+)=([^?&+#]+)/g;
      let obj = {};
      url.replace(reg, (res, $1, $2) => { obj[$1] = $2 });
      return obj;
    }

    //all参数默认空，是真时返回为数组
    static getElement(selector, all = false) {
      return new Promise((resolve, reject) => {
        let num = 0;
        let timer = setInterval(function () {
          num++
          let dom;
          if (!all) {
            dom = document.querySelector(selector);
            if (dom) {
              clearInterval(timer);
              resolve(dom);
            }
          } else {
            dom = document.querySelectorAll(selector);
            if (dom.length > 0) {
              clearInterval(timer);
              resolve(dom);
            }
          }

          if (num == 20) {
            clearInterval(timer);
            resolve(false);
          }
        }, 300);
      });
    }

    static toast(msg, duration) {
      duration = isNaN(duration) ? 3000 : duration;
      let toastDom = document.createElement('div');
      toastDom.innerHTML = msg;
      toastDom.style.cssText = 'padding:2px 15px;min-height: 36px;line-height: 36px;text-align: center;transform: translate(-50%);border-radius: 4px;color: rgb(255, 255, 255);position: fixed;top: 50%;left: 50%;z-index: 9999999;background: rgb(0, 0, 0);font-size: 16px;'
      document.body.appendChild(toastDom);
      setTimeout(function () {
        let d = 0.5;
        toastDom.style.webkitTransition = '-webkit-transform ' + d + 's ease-in, opacity ' + d + 's ease-in';
        toastDom.style.opacity = '0';
        setTimeout(function () { document.body.removeChild(toastDom) }, d * 1000);
      }, duration);
    }

    static download(url, filename) {
      let ua = navigator.userAgent.toLowerCase();
      console.log(ua.match(/version\/([\d.]+).*safari/));
      if (ua.match(/version\/([\d.]+).*safari/)) {
        window.open(url);
      } else {
        GM_download(url, filename);
      }
    }

    static parseHeaderString(str) {
      let headerArray = str.trim().split(/[\r\n]+/);
      let headers = {};
      headerArray.forEach(function (line) {
        let parts = line.split(': ');
        let header = parts.shift();
        let value = parts.join(': ');
        headers[header] = value;
      });
      return headers;
    }

    static escapeFileName(str) {
      return (''+str).trim().replace(/[\"\'\*\?\\\/<>|:]+/g, '');
    }
  };

  let AjaxInterceptors = [];

  // // 拦截 fetch 的响应
  // const originFetch = fetch;
  // window.unsafeWindow.fetch = function (url, options) {
  //   let xhr = this;
  //   return originFetch(url, options).then(async (response) => {
  //     AjaxInterceptors.forEach(function (item) {
  //       if (response.url.match(item.match)) {
  //         const responseClone = response.clone();
  //         item.onResponse(responseClone);
  //       }
  //     });
  //   });
  // };

  // 拦截 XMLHttpRequest 的响应
  const originOpen = XMLHttpRequest.prototype.open;
  XMLHttpRequest.prototype.open = function (_, url) {
    let xhr = this;
    AjaxInterceptors.forEach(function (item) {
      if (url.match(item.match)) {
        xhr.addEventListener("readystatechange", function () {
          if (xhr.readyState === 4) {
            let headers = Utils.parseHeaderString(xhr.getAllResponseHeaders());
            let response = {
              status: xhr.status,
              statusText: xhr.statusText,
              ok: xhr.status == 200,
              type: xhr.responseType,
              headers: headers,
              url: xhr.responseURL,
              body: xhr.response,
              bodyStr: xhr.responseText,
              text: function () {
                return this.bodyStr;
              },
              json: function () {
                try {
                  return JSON.parse(this.bodyStr);
                }
                catch (e) {
                  return null;
                }
              },
            };
            item.onResponse(response);
          }
        });
      }
    });
    originOpen.apply(this, arguments);
  };

  class VideoDownloadClass {
    kuaishou() {
      window.addEventListener('load', function () {
        async function getControls() {
          let videoDomArr = await Utils.getElement('.player-video', true);
          if (!videoDomArr) {
            console.log('没有找到DOM'); return;
          }
          let videoDom = videoDomArr.length > 2 ? videoDomArr[1] : videoDomArr[0];
          if (videoDom.getAttribute('src').match(/^blob/)) {
            //删除残留下载DOM
            Utils.toast('blob视频无法下载'); return;
          }

          let title = '视频';
          let titleDom = await Utils.getElement('.feed-caption');
          if (titleDom && titleDom.childNodes.length > 0) {
            title = Utils.escapeFileName(titleDom.childNodes[0].data);
          }

          let linksConfig = [
            { name: title, url: videoDom.getAttribute('src'), type: 'aria' },
          ];
          drawLinks(linksConfig);
        }

        getControls();

        document.addEventListener('click', function (e) {
          getControls();
        });
        window.addEventListener("wheel", getControls);
        window.addEventListener('keydown', function (e) {
          if (e.code == 'ArrowDown' || e.code == 'ArrowUp') {
            getControls();
          }
        });
      });
    }

    xigua() {
      window.addEventListener('load', function () {
        async function getControls() {
          let videoDom = await Utils.getElement('video');
          if (!videoDom) {
            console.log('没有找到DOM'); return;
          }

          GM_xmlhttpRequest({
            method: "get",
            url: 'http://47.99.158.118/video-crack/v2/parse?content=' + encodeURIComponent(location.href),
            data: '',
            headers: { 'Accept': 'text/plain, text/html,application/json' },
            onload: async function (res) {
              if (res.status == 200) {
                let resp = JSON.parse(res.responseText)
                if (resp.code != 0) {
                  Utils.toast('视频解析失败');
                  return;
                }

                let title = '视频';
                let titleDom = await Utils.getElement('h1');
                if (titleDom) {
                  title = Utils.escapeFileName(titleDom.innerText);
                }

                let linksConfig = [
                  { name: title, url: resp.data.url, type: 'aria' }
                ];
                drawLinks(linksConfig);
              }
            },
            onerror: function (err) {
              console.log('xigua.error', err)
            }
          });

          document.querySelector('video').addEventListener('ended', function () { //结束
            setTimeout(function () {
              location.reload();
            }, 5500);
          }, false);
        }

        getControls();
      })
    }

    youtube() {
      if (!domMain) return;
      let timer = setInterval(function () {
        let url = location.href.match(/^https?:\/\/www\.youtube\.com\/(watch\?v=.+|shorts\/.+)/);
        if (url) {
          let linksConfig = [
            { name: '下载线路1', url: 'https://zh.savefrom.net/176/#url=' + location.href },
            { name: '下载线路2', url: 'https://mydowndown.com/y2#' + location.href },
            { name: '下载线路3', url: 'https://www.ytdownfk.com/search?url=' + location.href },
            { name: '下载线路4', url: 'https://yout.com/video/?url=' + location.href }
          ];
          drawLinks(linksConfig);
        }
      }, 200);
    }

    cctv() {
      if (!domMain) return;
      /**
       * 响应处理
       * @param {Response} response
       */
      function onResponse(response) {
        if (response.status != 200) {
          console.log('请求失败');
          return;
        }
        try {
          let data = response.json();
          let linksConfig = [];
          let title = Utils.escapeFileName(data.title || '视频');
          let index = data.video.validChapterNum || 4;
          for (let i = index; i > 0; i--) {
            let chapters = data.video[`chapters${i}`]
            if (chapters) {
              for (let j = 0; j < chapters.length; j++) {
                let chapter = chapters[j];
                let url = chapter.url;
                if (!url) {
                  let tmp = chapter.image.split('/fmspic/');
                  if (tmp[1]) {
                    let file = tmp[1].split('-')[0];
                    url = `https://vod.cntv.lxdns.com/flash/mp4video63/TMS/${file}_h2642000000nero_aac16-${j + 1}.mp4`;
                  }
                }
                linksConfig.push({
                  name: `${title}-${j + 1}`,
                  url,
                  type: 'aria'
                });
              }
              break;
            }
          }
          drawLinks(linksConfig);
        }
        catch (e) {
          return;
        }
      };
      AjaxInterceptors = [
        { match: /getHttpVideoInfo\.do/, onResponse },
      ]
    }

    weixinMp() {
      if (!domMain) return;
      let timer = setTimeout(async function () {
        let title = '视频';
        let titleDom = await Utils.getElement('h1');
        if (titleDom) {
          title = Utils.escapeFileName(titleDom.innerText);
        }
        //获取所有视频
        let videoDomAll = await Utils.getElement('video', true);
        if (videoDomAll.length == 0) return;
        //获取所有视频
        let linksConfig = [];
        let index = 1;
        for (let i = 0; i < videoDomAll.length; i++) {
          let url = videoDomAll[i].getAttribute('src');
          if (!url || url.substring(0, 4) === 'blob') continue;
          linksConfig.push({
            name: `${title}-${index++}`,
            url: url,
            type: 'aria',
            headers: {
              'Accept': '*/*',
              'Accept-Language': 'zh-CN,zh;q=0.9,en-CN;q=0.8,en;q=0.7',
              'Cache-Control': 'no-cache',
              'Connection': 'keep-alive',
              'Origin': 'https://mp.weixin.qq.com',
              'Pragma': 'no-cache',
              'Range': 'bytes=0-',
              'Referer': 'https://mp.weixin.qq.com/',
              'Sec-Fetch-Dest': 'video',
              'Sec-Fetch-Mode': 'cors',
              'Sec-Fetch-Site': 'cross-site',
            }
          });
        }
        drawLinks(linksConfig);
        //   clearInterval(timer);
      }, 2000)
    }
  }

  // 绘制界面
  function drawInterface() {
    let interfaceStyle = `
      .hpy-video-downloader {
        z-index:999999;
        position:fixed;
        left:5px;
        top:${iconTop}px;
        overflow:visible;
        display:flex;
        width:auto;
      }
      .hpy-video-downloader-btn {
        background-color:#1296db;
        color:#fff;
        fill:#fff;
        border:0;
        height:${iconWidth}px;
      }
      .hpy-video-downloader-links {
        display:none;
        width:320px;
        max-height:316px;
        overflow-y:auto;
        overflow-x:hidden;
      }
      .hpy-video-downloader-link {
        display:inline-block;
        padding:4px 0px;
        margin:2px;
        width:30%;
        color:#FFF;
        text-align:center;
        background-color:#1296db;
        box-shadow:0px 0px 10px #fff;
        font-size:14px;
        cursor:pointer;
      }
      .hpy-video-downloader-link:hover {
        color:#fff;
        background-color:#2296fb;
      }
      .hpy-setting-dialog {
        z-index: 9999;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
      .hpy-setting-dialog-mask {
        width: 100%;
        height: 100%;
        background: rgba(0,0,0,.5);
      }
      .hpy-setting-dialog-content {
        position: absolute;
        top: 10vh;
        left: 25%;
        width: 50%;
        background: #fff;
        border-radius: 10px;
        font-size: 14px;
      }
      .hpy-setting-head {
        height: 50px;
        line-height: 50px;
        position: relative;
        background: #1296db;
        color: #fff;
        text-align: center;
        border-radius: 10px 10px 0 0;
      }
      .hpy-setting-title {
        font-size: 18px;
      }
      .hpy-setting-close {
        font-size: 18px;
        position: absolute;
        top: 0;
        right: 15px;
        padding: 0 10px;
        cursor: pointer;
      }
      .hpy-setting-body {
        padding: 15px;
      }
      .hpy-setting-foot {
        height: 50px;
        line-height: 50px;
        text-align: right;
        border-top: 1px solid #1296db;
      }
      .hpy-setting-cancel, .hpy-setting-confirm {
        height: 30px;
        line-height: 30px;
        text-align: center;
        font-size: 14px;
        padding: 0 15px;
        border: 1px solid #1296db;
        background: #fff;
        color: #1296db;
        border-radius: 4px;
        margin-right: 15px;
      }
      .hpy-setting-confirm {
        background: #1296db;
        color: #fff;
      }
      .hpy-setting-item {
        margin-bottom: 15px;
      }
      .hpy-setting-item:last-child {
        margin-bottom: 0;
      }
      .hpy-setting-item-desc {
        font-size: 14px;
        color: #666;
      }
      .hpy-setting-item-content {
        font-size: 14px;
        color: #333;
      }
      .hpy-setting-item-input {
        font-size: 12px;
        color: #333;
        padding: 0 10px;
        height: 30px;
        line-height: 30px;
        width: 100%;
        border: 1px solid #999;
        box-sizing: border-box;
        border-radius: 4px;
      }
      .hpy-setting-item-input:focus {
        border-color: #1296db;
      }
    `;

    domStyle.appendChild(document.createTextNode(interfaceStyle));
    domHead.appendChild(domStyle);
    let mainHtml = `
      <button class="hpy-video-downloader-btn">
        <svg width="${iconWidth}" height="${iconWidth}" t="1707278539161" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4248" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M511.84 93.867c225.959 0 411.01 179.648 417.795 405.878l0.185 11.946 0.313 20.22H834.47v-19.91c-0.001-176.864-142.295-320.782-319.084-322.726-176.789-1.943-322.21 138.81-326.097 315.631-3.887 176.82 135.21 323.833 311.583 329.657l11.294 0.185 19.578 0.321v95.064H511.84C280.993 930.133 93.856 742.93 93.856 512c0-230.929 187.137-418.133 417.983-418.133z m273.698 464.077c16.085 0 29.06 14.792 29.06 32.92V722.55h66.652c16.085 0 29.06 14.882 28.972 33.102-0.09 9.38-3.733 18.31-10.132 24.894h-0.089L775.23 902.308c-10.575 10.552-27.282 10.552-37.858 0L612.6 780.545c-12.175-12.447-13.508-32.47-3.021-46.541 5.243-7.125 13.33-11.455 21.95-11.455h66.919V590.865c0-18.22 12.974-32.921 29.06-32.921h58.031z m-340.263-208.66L676.799 488.2v47.602L445.275 674.715l-42.092-23.8v-277.83l42.092-23.8z" p-id="4249"></path></svg>
      </button>
      <div class="hpy-video-downloader-links" id="${id}Links"></div>
    `;
    let wrapper = document.createElement('div');
    wrapper.id = id;
    wrapper.className = 'hpy-video-downloader';
    wrapper.innerHTML = createHTML(mainHtml);
    document.body.appendChild(wrapper);
    domMain = document.querySelector(`#${id}`);
    domLinks = document.querySelector(`#${id}Links`);
    domMain.style.display = 'none';
    domMain.onmouseover = () => {
      domLinks.style.display = 'block';
    }
    domMain.onmouseout = () => {
      domLinks.style.display = 'none';
    }
  }
  function createHTML(html) {
    if (window.trustedTypes && window.trustedTypes.createPolicy && !window.trustedTypes.defaultPolicy) {
      let policy = window.trustedTypes.createPolicy('default', {
        createHTML: string => string
      });
      return policy.createHTML(html);
    }
    return html;
  }
  function drawSetting() {
    if (!domSetting) {
      let settingHtml = `
        <div class="hpy-setting-dialog-mask"></div>
        <div class="hpy-setting-dialog-content">
          <div class="hpy-setting-head">
            <h3 class="hpy-setting-title">参数设置</h3>
            <span class="hpy-setting-close">&times;</span>
          </div>
          <div class="hpy-setting-body">
            <div class="hpy-setting-item">
              <div class="hpy-setting-item-desc">RPC地址：</div>
              <div class="hpy-setting-item-content">
                <input class="hpy-setting-item-input" name="hpySettingRpcUrl" value="${Config.rpcUrl}" />
              </div>
            </div>
            <div class="hpy-setting-item">
              <div class="hpy-setting-item-desc">RPC密钥：</div>
              <div class="hpy-setting-item-content">
                <input class="hpy-setting-item-input" name="hpySettingRpcToken" value="${Config.rpcToken}" />
              </div>
            </div>
          </div>
          <div class="hpy-setting-foot">
            <button class="hpy-setting-cancel">取消</button>
            <button class="hpy-setting-confirm">保存</button>
          </div>
        </div>
      `;
      let dialog = document.createElement('div');
      dialog.className = 'hpy-setting-dialog';
      dialog.innerHTML = settingHtml;
      document.body.appendChild(dialog);
      domSetting = document.querySelector(`.hpy-setting-dialog`);

      const domClose = domSetting.querySelector(`.hpy-setting-close`);
      const domCancel = domSetting.querySelector(`.hpy-setting-cancel`);
      const domConfirm = domSetting.querySelector(`.hpy-setting-confirm`);
      domClose.addEventListener('click', function () {
        domSetting.style.display = 'none';
      });
      domCancel.addEventListener('click', function () {
        domSetting.style.display = 'none';
      });
      domConfirm.addEventListener('click', function () {
        let rpcUrl = domSetting.querySelector(`input[name=hpySettingRpcUrl]`).value;
        let rpcToken = domSetting.querySelector(`input[name=hpySettingRpcToken]`).value;
        Config.rpcUrl = rpcUrl;
        Config.rpcToken = rpcToken;
        GM_setValue('hpyVideoDownloaderRpcUrl', rpcUrl);
        GM_setValue('hpyVideoDownloaderRpcToken', rpcToken);
        domSetting.style.display = 'none';
      });
    }
    domSetting.style.display = '';

  }
  // 启用界面拖动
  function enableInterfaceDrag() {
    if (!domMain) return;
    domMain.onmousedown = function (event) {
      if (event.which == 3) return false;//屏蔽右键
      let sedownTop = domMain.offsetTop;
      let zhmLogoIconHeight = iconWidth;
      let bottomSpace = 10;
      if (event.target.className != 'iconLogo') return;
      //let shiftX = event.clientX - domMain.getBoundingClientRect().left;
      let shiftx = 5;
      let shiftY = event.clientY - domMain.getBoundingClientRect().top;
      domMain.style.position = 'fixed';
      domMain.style.zIndex = 9999999;
      document.body.append(domMain);

      function onMouseMove(event) {
        //domMain.style.left = pageX - shiftX + 'px';
        domMain.style.left = '5px';
        let height = window.innerHeight - zhmLogoIconHeight - bottomSpace;
        let y = event.pageY - shiftY;
        y = Math.min(Math.max(0, y), height);
        domMain.style.top = y + 'px';
      }
      //在mousemove事件上移动图标
      document.addEventListener('mousemove', onMouseMove);
      //松开事件
      document.onmouseup = function (e) {
        GM_setValue('iconTop', domMain.offsetTop);
        document.removeEventListener('mousemove', onMouseMove);
        domMain.onmouseup = null;
        let height = domMain.offsetTop + zhmLogoIconHeight + bottomSpace;
        if (domMain.offsetTop < 0) {
          domMain.style.top = '0px';
        }
        if (window.innerHeight < height) {
          domMain.style.top = window.innerHeight - zhmLogoIconHeight - bottomSpace + 'px';
        }
      };
    };
    domMain.ondragstart = function () {
      return false;
    };
  }
  // 绘制下载链接
  function drawLinks(linksConfig) {
    if (!linksConfig || linksConfig.length === 0) return;
    let linksHtml = '';
    linksConfig.forEach(function (item, index) {
      linksHtml += `<div class="hpy-video-downloader-link" data-index="${index}">${item.name}</div>`;
    });
    domLinks.innerHTML = linksHtml;

    let domLinkItems = document.querySelectorAll('.hpy-video-downloader-link');
    domLinkItems.forEach(function (domLinkItem) {
      domLinkItem.addEventListener('click', function () {
        let index = this.getAttribute('data-index');
        let item = linksConfig[index];
        if (item.type === 'download') {
          Utils.toast('视频下载中，请稍后...');
          Utils.download({
            url: item.url,
            name: `${item.name}.mp4`,
            headers: item.headers || {},
            onerror: function (e) {
              console.log('download.error', e);
            }
          });
        }
        else if (item.type === 'aria') {
          let headers = [];
          item.headers = item.headers || {};
          for (let key in item.headers) {
            headers.push(`${key}: ${item.headers[key]}`);
          }

          let rpcData = {
            id: new Date().getTime(),
            jsonrpc: '2.0',
            method: 'aria2.addUri',
            params: [`token:${Config.rpcToken}`, [item.url], {
              out: `${item.name}.mp4`,
              header: headers,
            }]
          };

          GM_xmlhttpRequest({
            method: 'post',
            url: Config.rpcUrl,
            data: JSON.stringify(rpcData),
            headers: item.headers,
            responseType: 'json',
            onload: function (res) {
              if (res.status == 200) {
                let resp = JSON.parse(res.responseText)
                if (!resp.result) {
                  Utils.toast('推送失败');
                  return;
                }
                Utils.toast('推送成功');
              }
            },
            onerror: function (err) {
              console.log('rpc.error', err)
              Utils.toast('RPC请求异常');
            }
          });
        }
        else {
          window.open(item.url, '_blank');
        }
      });
    });
    domMain.style.display = 'flex';
  }

  let domMain, domLinks, domSetting;
  drawInterface();
  let videoDownloadClass = new VideoDownloadClass();
  Webs.forEach(function (web) {
    let result = location.href.match(web.match);
    if (result) {
      videoDownloadClass[web.name]();
    }
  });

  GM_registerMenuCommand('参数设置', () => drawSetting());

})();
